-- 
-- Please see the license.html file included with this distribution for 
-- attribution and copyright information.
--

function onInit()
	update();
end

function updateControl(sControl, bReadOnly, bForceHide)
	if not self[sControl] then
		return false;
	end
	
	return self[sControl].update(bReadOnly, bForceHide);
end

function update()
	local bReadOnly = WindowManager.getReadOnlyState(getDatabaseNode());

	
	
	local sType = DB.getValue(getDatabaseNode(), "npctype", "");
	
	if sType=="Weak" or sType=="" then
		
		dividerattacks.setVisible(true);
		dividerabilities.setVisible(true);
		titlebar.setVisible(true);
		dividermagick.setVisible(true);
		dividerother.setVisible(true);
	
		updateControl("speed", bReadOnly);
		updateControl("strength", bReadOnly);
		updateControl("meleeattack", bReadOnly);
		updateControl("rangedattack", bReadOnly);
		updateControl("defense", bReadOnly);
		updateControl("armor", bReadOnly);
		updateControl("willpower", bReadOnly);
		updateControl("initiative", bReadOnly);
		updateControl("detect", bReadOnly);
		updateControl("sneak", bReadOnly);
		
		updateControl("vitality", bReadOnly);
		updateControl("wounds", false);
	
		updateControl("attacks", bReadOnly);
		if not attacks.isVisible() then
			dividerattacks.setVisible(false);
		end
		
		updateControl("abilities", bReadOnly);
		updateControl("skills", bReadOnly);
		if not abilities.isVisible() and not skills.isVisible() then
			dividerabilities.setVisible(false);
		end	
		
		updateControl("arcane", bReadOnly);
		updateControl("fatigueorfocus", bReadOnly);
		updateControl("tradition", bReadOnly);
		updateControl("spells", bReadOnly);
		if not spells.isVisible() then
			titlebar.setVisible(false);
		end
		if not arcane.isVisible() and not tradition.isVisible() and not spells.isVisible() then
			dividermagick.setVisible(false);
		end	
		
		updateControl("basesize", bReadOnly);
		updateControl("encounterpoints", bReadOnly);
		if not basesize.isVisible() and not encounterpoints.isVisible() then
			dividerother.setVisible(false);
		end

	end
	
	
	if sType=="Tough" then
		
		dividerattacks.setVisible(true);
		dividerabilities.setVisible(true);
		if titlebar then titlebar.setVisible(true); end
		if dividermagick then dividermagick.setVisible(true); end
		dividerother.setVisible(true);
	
		updateControl("speed", bReadOnly);
		updateControl("strength", bReadOnly);
		updateControl("meleeattack", bReadOnly);
		updateControl("rangedattack", bReadOnly);
		updateControl("defense", bReadOnly);
		updateControl("armor", bReadOnly);
		updateControl("willpower", bReadOnly);
		updateControl("initiative", bReadOnly);
		updateControl("detect", bReadOnly);
		updateControl("sneak", bReadOnly);
		
		updateControl("physique", bReadOnly);
		updateControl("agility", bReadOnly);
		updateControl("intellect", bReadOnly);
	
		updateControl("attacks", bReadOnly);
		if not attacks.isVisible() then
			dividerattacks.setVisible(false);
		end
		
		updateControl("abilities", bReadOnly);
		updateControl("skills", bReadOnly);
		if not abilities.isVisible() and not skills.isVisible() then
			dividerabilities.setVisible(false);
		end	
		
		updateControl("arcane", bReadOnly);
		updateControl("fatigueorfocus", bReadOnly);
		updateControl("tradition", bReadOnly);
		updateControl("spells", bReadOnly);
		if not spells.isVisible() then
			titlebar.setVisible(false);
		end
		if not arcane.isVisible() and not tradition.isVisible() and not spells.isVisible() then
			dividermagick.setVisible(false);
		end	
		
		updateControl("basesize", bReadOnly);
		updateControl("encounterpoints", bReadOnly);
		if not basesize.isVisible() and not encounterpoints.isVisible() then
			dividerother.setVisible(false);
		end

	end
	
	
	if sType=="Steamjack" then
		
		dividerattacks.setVisible(true);
		dividerabilities.setVisible(true);
		
	
		updateControl("speed", bReadOnly);
		updateControl("strength", bReadOnly);					
		updateControl("prowess", bReadOnly);
		updateControl("poise", bReadOnly);
		updateControl("perception", bReadOnly);
		updateControl("physique", bReadOnly);
		updateControl("agility", bReadOnly);
		updateControl("intellect", bReadOnly);
				
		updateControl("meleeattack", bReadOnly);
		updateControl("rangedattack", bReadOnly);
		updateControl("defense", bReadOnly);
		updateControl("armor", bReadOnly);
		updateControl("willpower", bReadOnly);
		updateControl("initiative", bReadOnly);	
		
		updateControl("cortex", bReadOnly);
		updateControl("fuelburn", bReadOnly);
		updateControl("tonnage", bReadOnly);		
		updateControl("specialrules", bReadOnly);	
		
		
		if track1 then track1.setVisible(not bReadOnly); end
		if track2 then track2.setVisible(not bReadOnly); end
		if track3 then track3.setVisible(not bReadOnly); end
		if track4 then track4.setVisible(not bReadOnly); end
		if track5 then track5.setVisible(not bReadOnly); end
		if track6 then track6.setVisible(not bReadOnly); end
	
		updateControl("attacks", bReadOnly);
		if not attacks.isVisible() then
			dividerattacks.setVisible(false);
		end
		
		updateControl("abilities", bReadOnly);		
		if not abilities.isVisible() then
			dividerabilities.setVisible(false);
		end	
		
		

	end
					
	
end

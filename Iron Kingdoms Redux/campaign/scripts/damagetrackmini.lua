-- 
-- Please see the license.html file included with this distribution for 
-- attribution and copyright information.
--


local container={ physique={}, agility={}, intellect={} };


local sizeBox = { x=7, y=7 };


local posBox=
{
	
	physique={
	{x=106,y=23},
	{x=98,y=23},	
	{x=90,y=19},	
	{x=90,y=27},
	{x=82,y=19},	
	{x=82,y=27},	
	{x=74,y=19},	
	{x=74,y=27},	
	{x=66,y=19},	
	{x=66,y=27},
	{x=58,y=19},
	{x=58,y=27},
	{x=50,y=19},
	{x=50,y=27}
	},
	
	agility={
	{x=106,y=43},
	{x=98,y=43},	
	{x=90,y=39},	
	{x=90,y=47},
	{x=82,y=39},	
	{x=82,y=47},	
	{x=74,y=39},	
	{x=74,y=47},	
	{x=66,y=39},	
	{x=66,y=47},
	{x=58,y=39},
	{x=58,y=47},
	{x=50,y=39},
	{x=50,y=47}
	},
	
	intellect={
	{x=106,y=63},
	{x=98,y=63},	
	{x=90,y=59},	
	{x=90,y=67},
	{x=82,y=59},	
	{x=82,y=67},	
	{x=74,y=59},	
	{x=74,y=67},	
	{x=66,y=59},	
	{x=66,y=67},
	{x=58,y=59},
	{x=58,y=67},
	{x=50,y=59},
	{x=50,y=67}
	}
	
};




function onInit()
	local sNode = window.getDatabaseNode().getNodeName();
	DB.addHandler(sNode , "onChildUpdate", updateSlots);		
	updateSlots();		
end

function onClose()
	local sNode = window.getDatabaseNode().getNodeName();	
	DB.removeHandler(sNode , "onChildUpdate", updateSlots);	
end












function createTracks(track)
	
	vitality=window[track].getValue();	
	
	
	if (vitality>14) then vitality=14; end;
	
	local track1= math.ceil( math.max(vitality-2,0) / 2);
	local track2= math.max(vitality-2-track1,0);
	
	
	
	
	if #container[track] ~= vitality then
		-- Clear
		for _,v in ipairs(container[track]) do
				v.destroy();
			end
			container[track] = {};
			
		
		for i = 1, vitality do
			
				local leadingzero="";
				if i<10 then leadingzero="0"; end
				
				local control=nil;
				control=window.createControl("minidamagebox".."_"..track,"damage."..track..".box"..leadingzero..i);										
				control.setAnchor( "top", "damagetrack", "top" ,"absolute",posBox[track][i].y) ;									
				control.setAnchor( "left", "damagetrack", "left" ,"absolute",posBox[track][i].x) ;									
				control.setAnchoredWidth(sizeBox.x);
				control.setAnchoredHeight(sizeBox.y);				
			
				
			container[track][i]=control;
		end
	
	end
	
end






function boxClicked(track)
	local allpressed=true;
	for _,v in ipairs(container[track]) do
		if (v.getValue()==0) then allpressed=false; end;		
	end
	
	
end

function updateSlots()
			
	createTracks("physique");
	createTracks("agility");
	createTracks("intellect");
	
end



-- 
-- Please see the license.html file included with this distribution for 
-- attribution and copyright information.
--

function onInit()	
	local sNode = getDatabaseNode().getNodeName();
	DB.addHandler(sNode .. ".locked", "onUpdate", onLockChanged);	
	onLockChanged();
end

function onClose()		
	local sNode = getDatabaseNode().getNodeName();
	DB.removeHandler(sNode .. ".locked", "onUpdate", onLockChanged);	
end

function onLockChanged()
	StateChanged();
end


function StateChanged()
	if header.subwindow then
		header.subwindow.update();
	end
	if main.subwindow then
		main.subwindow.update();
	end
end

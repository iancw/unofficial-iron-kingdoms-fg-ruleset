function onInit()
	registerMenuItem(Interface.getString("list_menu_createitem"), "insert", 5);
end

function updateArmorValue()
	local armor=getWindows();
	if armor[1]~=nil then
		armor[1].updateArmor();
	else
		window.armormod_armor.setValue(0);						
		window.defensemod_armor.setValue(0);						
		window.speedmod_armor.setValue(0);	
	end
end

function onMenuSelection(item)
	if item == 5 then
		win=addEntry(true);		
	end
end

function onListChanged()
	updateArmorValue();
end

function addEntry(bFocus)
	
	local items=getDatabaseNode().getChildren();
	for k,v in pairs(items) do
		v.delete();
	end
	
	local w = createWindow();
	if bFocus and w  then
		w.name.setFocus();
	end
	w.locktype.setValue(1);
	return w;
end

	

function onDrop(x, y, draginfo)

	
	local sDragType = draginfo.getType();
	
	if sDragType ~= "shortcut" then
		return false;
	end
	
	local sDropClass, sDropNodeName = draginfo.getShortcutData();
	if not StringManager.contains({"item", "referencearmor"}, sDropClass) then
		return true;
	end
	
	dropType=DB.getValue(draginfo.getDatabaseNode(), "type", "");
	
	if not Utility.contains(dropType,"Armor")  then
		return true
	end
	
	--[[
	if not  StringManager.contains({"Armor"}, dropType) then
		return true;
	end
	]]--
	
	local nodeSource = draginfo.getDatabaseNode();
	local nodeTarget = addEntry(false).getDatabaseNode();
		
	local sSourceType = DB.getValue(nodeSource, "type", "");
	local sTargetType = DB.getValue(nodeTarget, "type", "");
	
	local sSourceName = DB.getValue(nodeSource, "name", "");
	sSourceName = string.gsub(sSourceName, " %(" .. sSourceType .. "%)", "");
	local sTargetName = DB.getValue(nodeTarget, "name", "");
	sTargetName = string.gsub(sTargetName, " %(" .. sTargetType .. "%)", "");
	
	--if StringManager.contains({ "Armor"--[[, "Armor"]] }, sSourceType) then
		
			DB.copyNode( nodeSource, nodeTarget ) ;
			DB.setValue(nodeTarget, "locked", "number", 1, 0);
			--[[
			if sSourceName ~= "" then
				local sName = sSourceName .. " (" .. DB.getValue(nodeTarget, "name", "") .. ")";
				DB.setValue(nodeTarget, "name", "string", sName);
				DB.setValue(nodeTarget, "nonid_name", "string", DB.getValue(nodeSource, "nonid_name", ""));
				DB.setValue(nodeTarget, "nonidentified", "string", DB.getValue(nodeSource, "nonidentified", ""));				
				DB.setValue(nodeTarget, "isidentified", "number", DB.getValue(nodeSource, "isidentified", 0));
			end
			
			
			DB.setValue(nodeTarget, "locked", "number", 1, 0);
			
			DB.setValue(nodeTarget, "name", "string", DB.getValue(nodeSource, "name", ""));
			DB.setValue(nodeTarget, "type", "string", DB.getValue(nodeSource, "type", ""));
			DB.setValue(nodeTarget, "cost", "string", DB.getValue(nodeSource, "cost", ""));
			
			
			--if sSourceType == "Armor" then				
				DB.setValue(nodeTarget, "speedmod", "number", DB.getValue(nodeSource, "speedmod", 0));
				DB.setValue(nodeTarget, "defensemod", "number", DB.getValue(nodeSource, "defensemod", 0));
				DB.setValue(nodeTarget, "armormod", "number", DB.getValue(nodeSource, "armormod", 0));				
				DB.setValue(nodeTarget, "description", "formattedtext", DB.getValue(nodeSource, "description", ""));	
				DB.setValue(nodeTarget, "special", "string", DB.getValue(nodeSource, "special", ""));
				DB.setValue(nodeTarget, "special_formatted", "formattedtext", DB.getValue(nodeSource, "special_formatted", ""));
			--end
		--end
		]]--
	
	return true;
end
				
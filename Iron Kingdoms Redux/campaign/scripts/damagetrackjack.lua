-- 
-- Please see the license.html file included with this distribution for 
-- attribution and copyright information.
--


local container={ track1={}, track2={}, track3={}, track4={}, track5={}, track6={} };
local powerfield={};

local sizeBox = { x=11, y=11 };


local posBox=
{
	
	track1={
	{x=7,y=90},
	{x=7,y=78},	
	{x=7,y=66},	
	{x=7,y=54},
	{x=7,y=42}
	
	},
	
	track2={
	{x=19,y=102},
	{x=19,y=90},	
	{x=19,y=78},	
	{x=19,y=66},
	{x=19,y=54},	
	{x=19,y=42}
	
	},
	
	track3={
	{x=31,y=102},
	{x=31,y=90},	
	{x=31,y=78},	
	{x=31,y=66},
	{x=31,y=54},	
	{x=31,y=42}
	},
	
	track4={
	{x=43,y=102},
	{x=43,y=90},	
	{x=43,y=78},	
	{x=43,y=66},
	{x=43,y=54},	
	{x=43,y=42}
	},
	
	track5={
	{x=55,y=102},
	{x=55,y=90},	
	{x=55,y=78},	
	{x=55,y=66},
	{x=55,y=54},	
	{x=55,y=42}
	},
	
	track6={	
	{x=67,y=90},	
	{x=67,y=78},	
	{x=67,y=66},
	{x=67,y=54},	
	{x=67,y=42}
	}
	
};




function onInit()
	local sNode = window.getDatabaseNode().getNodeName();
	DB.addHandler(sNode .. ".tracks", "onChildUpdate", updateSlots);		
	updateSlots();		
end

function onClose()
	local sNode = window.getDatabaseNode().getNodeName();	
	DB.removeHandler(sNode .. ".tracks", "onChildUpdate", updateSlots);	
end










boxtypes={ track1={}, track2={}, track3={}, track4={}, track5={}, track6={} };

function createTracks(track)
	
	vitality=window[track].getValue();	
	
	
	if vitality > tonumber(window[track].max[1]) then
		vitality=tonumber(window[track].max[1]);
	end
	
	
	
	
	
	
	
	if #container[track] ~= vitality then
		-- Clear
		for _,v in ipairs(container[track]) do
			v.destroy();
		end
		container[track] = {};
		
		for _,v in ipairs(boxtypes[track]) do
			v.destroy();
		end
		boxtypes[track] = {};
			
		
		for i = 1, vitality do
			
				local leadingzero="";
				if i<10 then leadingzero="0"; end
				
				local control=nil;
				control=window.createControl("jackdmgbox","damage."..track..".box"..leadingzero..i);										
				control.setAnchor( "top", "damagetrack", "top" ,"absolute",posBox[track][i].y) ;									
				control.setAnchor( "left", "damagetrack", "left" ,"absolute",posBox[track][i].x) ;									
				control.setAnchoredWidth(sizeBox.x);
				control.setAnchoredHeight(sizeBox.y);			

				local boxtype=nil;
				boxtype=window.createControl("hn","damage."..track..".box"..leadingzero..i.."boxtype");										
				
			container[track][i]=control;
			boxtypes[track][i]=boxtype;
			
		end
	
	end
	
end






function boxClicked(track)
	local allpressed=true;
	for _,v in ipairs(container[track]) do
		if (v.getValue()==0) then allpressed=false; end;		
	end
	
	
end

function updateSlots()
			
	createTracks("track1");
	createTracks("track2");
	createTracks("track3");
	createTracks("track4");
	createTracks("track5");
	createTracks("track6");
	
	
end



-- 
-- Please see the license.html file included with this distribution for 
-- attribution and copyright information.
--

function onInit()
	update();
	
end

function VisDataCleared()
	update();
end

function InvisDataAdded()
	update();
end

function updateControl(sControl, bReadOnly, bID)
	if not self[sControl] then
		return false;
	end
		
	if not bID then
		return self[sControl].update(bReadOnly, true);
	end
	
	return self[sControl].update(bReadOnly);
end



function update()
	
	
	local nodeRecord = getDatabaseNode();
	local bReadOnly = WindowManager.getReadOnlyState(nodeRecord);
	local bLockType= (locktype.getValue()==1);
	local bID, bOptionID = ItemManager.getIDState(nodeRecord);
	
	local sType = type.getValue();
	
	--[[
	local bMeleeWeapon = (sType == "Melee Weapon");	
	local bRangedWeapon = (sType == "Ranged Weapon");
	local bWeapon= bMeleeWeapon or bRangedWeapon;		
	local bArmor = (sType == "Armor");
	local bHousing = (sType == "Housing");
	local bCapacitor = (sType == "Capacitor");
	local bRune = (sType == "Rune");
	]]
	
	local bMeleeWeapon = Utility.contains(sType,"Melee Weapon");	
	local bRangedWeapon = Utility.contains(sType,"Ranged Weapon");		
	local bWeapon= bMeleeWeapon or bRangedWeapon;		
	local bArmor = Utility.contains(sType,"Armor");		
	local bHousing =Utility.contains(sType,"Housing");		
	local bCapacitor =Utility.contains(sType,"Capacitor");		
	local bRune = Utility.contains(sType,"Rune");		
	local bDedicatedMechanika = Utility.contains(sType,"Dedicated Mechanika");
	local bAlchemical = Utility.contains(sType,"Alchemical");
	local bSteamjack = Utility.contains(sType,"Steamjack");
	
	local bSection1 = false;
	if bOptionID and User.isHost() then
		if updateControl("nonid_name", bReadOnly, true) then bSection1 = true; end;
	else
		updateControl("nonid_name", false);
	end
	if bOptionID and (User.isHost() or not bID) then
		if updateControl("nonidentified", bReadOnly, true) then bSection1 = true; end;
	else
		updateControl("nonidentified", false);
	end

	local bSection2 = false;
	--print(bLockType);
	--print(locktype.getValue());
	if updateControl("type", bReadOnly or bLockType, bID) then bSection2 = true; end	
	
	local bSection3 = false;
	if updateControl("cost", bReadOnly, bID) then bSection3 = true; end
	
	
	local bSection4 = false;
	--[[
	]]
	if updateControl("location", bReadOnly, bID and bSteamjack and bWeapon) then bSection4 = true; end
	
	if updateControl("maxammo", bReadOnly, bID and bRangedWeapon) then bSection4 = true; end
	if updateControl("ammotype", bReadOnly, bID and bRangedWeapon) then bSection4 = true; end
	if updateControl("range", bReadOnly, bID and bRangedWeapon) then bSection4 = true; end
	if updateControl("rangetext", bReadOnly, bID and bRangedWeapon) then bSection4 = true; end
	if updateControl("extremerange", bReadOnly, bID and bRangedWeapon) then bSection4 = true; end
	if updateControl("extremerangetext", bReadOnly, bID and bRangedWeapon) then bSection4 = true; end
	
	if updateControl("skill", bReadOnly, bID and bWeapon) then bSection4 = true; end
	if updateControl("attack", bReadOnly, bID and bWeapon) then bSection4 = true; end
	if updateControl("power", bReadOnly, bID and bWeapon) then bSection4 = true; end
	
	if updateControl("poweroutput", bReadOnly, bID and bCapacitor) then bSection4 = true; end
	if updateControl("lifespan", bReadOnly, bID and bCapacitor) then bSection4 = true; end
	
	if updateControl("runetype", bReadOnly, bID and bRune) then bSection4 = true; end
	if updateControl("runepoints", bReadOnly, bID and (bRune or bDedicatedMechanika)) then bSection4 = true; end
	
	powerapplystr.setReadOnly(bReadOnly);
	powerapplystr.setVisible(bID and bWeapon);
	powerapplystr_label.setVisible(bID and bWeapon);
	
	--if updateControl("powerextra", bReadOnly, bID and bWeapon ) then bSection4 = true; end
	if updateControl("damagetype", bReadOnly, bID and bWeapon ) then bSection4 = true; end
	
	if updateControl("aoe", bReadOnly, bID and bRangedWeapon) then bSection4 = true; end
				
	
	if updateControl("speedmod", bReadOnly, bID and bArmor) then bSection4 = true; end
	if updateControl("defensemod", bReadOnly, bID and bArmor) then bSection4 = true; end
	if updateControl("armormod", bReadOnly, bID and bArmor) then bSection4 = true; end
			
	local bSection5 = false;
	if updateControl("description", bReadOnly, bID) then bSection5 = true; end
	--description.setVisible(bID);
	--description.setReadOnly(bReadOnly);
	
	local bSection6 = false;
	if updateControl("special", bReadOnly, bID and (bRangedWeapon or bMeleeWeapon or bArmor)) then bSection6 = true; end
	if updateControl("special_formatted", bReadOnly, bID and (bRangedWeapon or bMeleeWeapon or bArmor or bCapacitor or bDedicatedMechanika or bAlchemical or bSteamjack)) then bSection6 = true; end
	
	local bSection7 = false;	
	if updateControl("fabricationreq", bReadOnly, bID and (bCapacitor or bHousing) ) then bSection7 = true; end
	if updateControl("fabricationcost", bReadOnly, bID and (bCapacitor or bHousing)) then bSection7 = true; end
	if updateControl("fabrication", bReadOnly, bID and (bCapacitor or bHousing or bDedicatedMechanika)) then bSection7 = true; end
	
	
	local bSection8 = false;	
	if updateControl("brewingreq", bReadOnly, bID and bAlchemical ) then bSection8 = true; end
	if updateControl("ingredients", bReadOnly, bID and bAlchemical) then bSection8 = true; end
	if updateControl("materialcost", bReadOnly, bID and bAlchemical) then bSection8 = true; end
	if updateControl("formula", bReadOnly, bID and bAlchemical) then bSection8 = true; end
	
	divider.setVisible(bSection1 and bSection2);
	divider2.setVisible((bSection1 or bSection2) and bSection3);
	divider3.setVisible((bSection1 or bSection2 or bSection3) and bSection4);
	divider4.setVisible((bSection1 or bSection2 or bSection3 or bSection4) and bSection5);
	divider5.setVisible((bSection1 or bSection2 or bSection3 or bSection4 or bSection5) and bSection6);
	divider6.setVisible((bSection1 or bSection2 or bSection3 or bSection4 or bSection5 or bSection6) and bSection7);
	divider7.setVisible((bSection1 or bSection2 or bSection3 or bSection4 or bSection5 or bSection6 or Section7) and bSection8);
	
	
	
end

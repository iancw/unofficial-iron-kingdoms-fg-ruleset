-- 
-- Please see the license.html file included with this distribution for 
-- attribution and copyright information.
--

function onInit()
	onIDChanged();	
	locked.onValueChanged();
	--toggleDetail();
end

function onIDChanged()
	local bID = ItemManager.getIDState(getDatabaseNode());
	name.setVisible(bID);
	nonid_name.setVisible(not bID);
end
			
function onTypeChanged()
	toggleDetail();
end

function onSkillUpdate()
	--stat.update(statname.getStringValue());
end

function isCustom()
	return iscustom.getValue()==0;
end

function replacetext(source, find, replace, wholeword)
  if wholeword then
    find = '%f[%a]'..find..'%f[%A]'
  end
  return (source:gsub(find,replace))
end

function toggleDetail()
	local ranged = (typeswitch.getValue() == 1);	
	local status = (activatedetail.getValue() == 1);
	
	-- melee will add strength to damage
	if(ranged) then
		type.setValue("Ranged");
		powerapplystr.setValue(0);
	else
		type.setValue("Melee");
		powerapplystr.setValue(1);
	end
	
	if special then
		label_special.setVisible(status);
		special.setVisible(status);
	end
	
	if special_formatted then
		special_formatted.setVisible(status);
		label_range.setVisible(ranged);		
	end
	
	range.setVisible(ranged);	
	if label_skill ~= nil then
		label_skill.setVisible(status);
	end
	skill.setVisible(status);
	label_extremerange.setVisible(status and ranged);
	extremerange.setVisible(status and ranged);
	label_aoe.setVisible(status and ranged);
	aoe.setVisible(status and ranged);
	label_ammo.setVisible(status and ranged);
	maxammo.setVisible(status and ranged);
	ammocounter.setVisible(status and ranged);
		
end

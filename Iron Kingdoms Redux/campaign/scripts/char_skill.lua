-- 
-- Please see the license.html file included with this distribution for 
-- attribution and copyright information.
--

function onInit()	
	
	onStatUpdate();
	setCustom()
end

function onStatUpdate()
	stat.update(statname.getStringValue());
end

-- This function is called to set the entry to non-custom or custom.
-- Custom entries have configurable stats and editable labels.
function setCustom()
	
	
	if isCustom() then
		label.setEnabled(true);
		label.setLine(true);
		
		statname.setStateFrame("hover", "sheetfocus", 5, 5, 5, 5);
		statname.setReadOnly(false);
	elseif iscustom.getValue()==2 then
		label.setEnabled(false);
		label.setLine(false);
		
		statname.setStateFrame("hover", "sheetfocus", 5, 5, 5, 5);
		statname.setReadOnly(false);
	else
		label.setEnabled(false);
		label.setLine(false);
		
		statname.setStateFrame("hover", nil);
		statname.setReadOnly(true);
	end
		
		if (label.getValue() == "Craft" or 
			label.getValue() == "Lore") then
			sublabel.setVisible(true);
		end
	
	
end

function isCustom()
	return iscustom.getValue()==0;
end


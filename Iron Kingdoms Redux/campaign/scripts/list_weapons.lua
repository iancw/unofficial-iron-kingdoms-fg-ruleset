function onInit()
	registerMenuItem(Interface.getString("list_menu_createitem"), "insert", 5);
end

function addEntry(bFocus)
	local w = createWindow();
	if bFocus and w  then
		w.name.setFocus();
	end
	w.locktype.setValue(1);
	return w;
end

function onMenuSelection(item)
	if item == 5 then
		win=addEntry(true);		
	end
end

function onDrop(x, y, draginfo)
	local sDragType = draginfo.getType();
	
	if sDragType ~= "shortcut" then
		return false;
	end
	
	local sDropClass, sDropNodeName = draginfo.getShortcutData();
	if not StringManager.contains({"item", "referenceweapon"}, sDropClass) then
		return true;
	end
	
	dropType=DB.getValue(draginfo.getDatabaseNode(), "type", "");
	
	if not (Utility.contains(dropType,"Ranged") or Utility.contains(dropType,"Melee")) then
		return true
	end
	
	--[[
	if not  (StringManager.contains({"Ranged Weapon"}, dropType) or StringManager.contains({"Melee Weapon"}, dropType) ) then
		return true;
	end
	]]--
	
	local nodeSource = draginfo.getDatabaseNode();
	local nodeTarget = addEntry(false).getDatabaseNode();
		
	local sSourceType = DB.getValue(nodeSource, "type", "");
	local sTargetType = DB.getValue(nodeTarget, "type", "");
	
	local sSourceName = DB.getValue(nodeSource, "name", "");
	sSourceName = string.gsub(sSourceName, " %(" .. sSourceType .. "%)", "");
	local sTargetName = DB.getValue(nodeTarget, "name", "");
	sTargetName = string.gsub(sTargetName, " %(" .. sTargetType .. "%)", "");
	
	
	--if StringManager.contains({ "Ranged Weapon", "Melee Weapon"--[[, "Armor"]] }, sSourceType) then
		
			DB.copyNode( nodeSource, nodeTarget ) ;
			DB.setValue(nodeTarget, "locked", "number", 1, 0);
			if Utility.contains(sSourceType,"Ranged") then
				DB.setValue(nodeTarget, "typeswitch", "number", 1, 0);
			else
				DB.setValue(nodeTarget, "typeswitch", "number", 0, 0);
			end
			
			--[[
			if sSourceName ~= "" then
				local sName = sSourceName .. " (" .. DB.getValue(nodeTarget, "name", "") .. ")";
				DB.setValue(nodeTarget, "name", "string", sName);
				DB.setValue(nodeTarget, "nonid_name", "string", DB.getValue(nodeSource, "nonid_name", ""));
				DB.setValue(nodeTarget, "nonidentified", "string", DB.getValue(nodeSource, "nonidentified", ""));				
				DB.setValue(nodeTarget, "isidentified", "number", DB.getValue(nodeSource, "isidentified", 0));
			end
			
			
			DB.setValue(nodeTarget, "locked", "number", 1, 0);
			
			DB.setValue(nodeTarget, "name", "string", DB.getValue(nodeSource, "name", ""));
			DB.setValue(nodeTarget, "type", "string", DB.getValue(nodeSource, "type", ""));
			DB.setValue(nodeTarget, "cost", "string", DB.getValue(nodeSource, "cost", ""));
			
			--if sSourceType == "Ranged Weapon" then
			if contains(sSourceType,"Ranged Weapon") then
				DB.setValue(nodeTarget, "typeswitch", "number", 1, 0);
				DB.setValue(nodeTarget, "maxammo", "number", DB.getValue(nodeSource, "maxammo", 0));
				DB.setValue(nodeTarget, "ammotype", "string", DB.getValue(nodeSource, "ammotype", ""));
				DB.setValue(nodeTarget, "range", "number", DB.getValue(nodeSource, "range", 0));
				DB.setValue(nodeTarget, "rangetext", "string", DB.getValue(nodeSource, "rangetext", ""));
				DB.setValue(nodeTarget, "extremerange", "number", DB.getValue(nodeSource, "extremerange", 0));
				DB.setValue(nodeTarget, "extremerangetext", "string", DB.getValue(nodeSource, "extremerangetext", ""));
				DB.setValue(nodeTarget, "skill", "string", DB.getValue(nodeSource, "skill", ""));
				DB.setValue(nodeTarget, "attack", "number", DB.getValue(nodeSource, "attack", 0));
				DB.setValue(nodeTarget, "power", "number", DB.getValue(nodeSource, "power", 0));
				DB.setValue(nodeTarget, "powerapplystr", "number", DB.getValue(nodeSource, "powerapplystr", ""));
				--DB.setValue(nodeTarget, "powerextra", "string", DB.getValue(nodeSource, "powerextra", ""));								
				DB.setValue(nodeTarget, "damagetype", "string", DB.getValue(nodeSource, "damagetype", ""));
				DB.setValue(nodeTarget, "aoe", "number", DB.getValue(nodeSource, "aoe", 0));
				DB.setValue(nodeTarget, "description", "formattedtext", DB.getValue(nodeSource, "description", ""));
				DB.setValue(nodeTarget, "special", "string", DB.getValue(nodeSource, "special", ""));
				DB.setValue(nodeTarget, "special_formatted", "formattedtext", DB.getValue(nodeSource, "special_formatted", ""));
			--elseif sSourceType == "Melee Weapon" then
			elseif contains(sSourceType,"Melee Weapon") then
				DB.setValue(nodeTarget, "typeswitch", "number", 0, 0);
				DB.setValue(nodeTarget, "skill", "string", DB.getValue(nodeSource, "skill", ""));
				DB.setValue(nodeTarget, "attack", "number", DB.getValue(nodeSource, "attack", 0));
				DB.setValue(nodeTarget, "power", "number", DB.getValue(nodeSource, "power", 0));
				DB.setValue(nodeTarget, "powerapplystr", "number", DB.getValue(nodeSource, "powerapplystr", ""));
				--DB.setValue(nodeTarget, "powerextra", "string", DB.getValue(nodeSource, "powerextra", ""));
				DB.setValue(nodeTarget, "damagetype", "string", DB.getValue(nodeSource, "damagetype", ""));
				DB.setValue(nodeTarget, "description", "formattedtext", DB.getValue(nodeSource, "description", ""));
				DB.setValue(nodeTarget, "special", "string", DB.getValue(nodeSource, "special", ""));
				DB.setValue(nodeTarget, "special_formatted", "formattedtext", DB.getValue(nodeSource, "special_formatted", ""));
			--elseif sSourceType == "Armor" then
			elseif contains(sSourceType,"Armor") then
				DB.setValue(nodeTarget, "speedmod", "number", DB.getValue(nodeSource, "speedmod", 0));
				DB.setValue(nodeTarget, "defensemod", "number", DB.getValue(nodeSource, "defensemod", 0));
				DB.setValue(nodeTarget, "armormod", "number", DB.getValue(nodeSource, "armormod", 0));				
				DB.setValue(nodeTarget, "description", "formattedtext", DB.getValue(nodeSource, "description", ""));	
				DB.setValue(nodeTarget, "special", "string", DB.getValue(nodeSource, "special", ""));
				DB.setValue(nodeTarget, "special_formatted", "formattedtext", DB.getValue(nodeSource, "special_formatted", ""));
			end
		]]--			
		--end
	return true;
end


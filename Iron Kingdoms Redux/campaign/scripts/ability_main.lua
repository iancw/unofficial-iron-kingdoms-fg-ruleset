-- 
-- Please see the license.html file included with this distribution for 
-- attribution and copyright information.
--

function onInit()
	update();
end

function VisDataCleared()
	update();
end

function InvisDataAdded()
	update();
end





function updateControl(sControl, bReadOnly)
	if not self[sControl] then
		return false;
	end
		
	
	
	return self[sControl].update(bReadOnly);
end

function update()
	
	
	local nodeRecord = getDatabaseNode();
	local bReadOnly = WindowManager.getReadOnlyState(getDatabaseNode());
							
	local bSection1 = false;
	if updateControl("type", bReadOnly) then bSection1 = true; end			
	if updateControl("prerequisites", bReadOnly) then bSection1 = true; end
	
	
	local bSection2 = false;
	if updateControl("benefit", bReadOnly) then bSection2 = true; end
		
	divider.setVisible(bSection1 and bSection2);
	
	
	
	
end

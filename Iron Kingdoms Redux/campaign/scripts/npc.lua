-- 
-- Please see the license.html file included with this distribution for 
-- attribution and copyright information.
--

function onInit()
	local sNode = getDatabaseNode().getNodeName();
	--for _,v in pairs(DataCommon.abilities) do
	--	DB.addHandler(sNode .. "." .. v, "onUpdate", updateAbility);
	--end
	DB.addHandler(sNode .. ".locked", "onUpdate", onLockChanged);

	TypeChanged();
	--updateAbility();
	onLockChanged();
end

function onClose()
	local sNode = getDatabaseNode().getNodeName();
	--for _,v in pairs(DataCommon.abilities) do
	--	DB.removeHandler(sNode .. "." .. v, "onUpdate", updateAbility);
	--end
	DB.removeHandler(sNode .. ".locked", "onUpdate", onLockChanged);
end


function TypeChanged()
	
	local sType = DB.getValue(getDatabaseNode(), "npctype", "");
	
	if sType == "Steamjack" then
		tabs.setTab(1, "main_steamjack", "tab_main");
	elseif sType == "Tough" then
		tabs.setTab(1, "main_tough", "tab_main");
	else
		tabs.setTab(1, "main_weak", "tab_main");
	end
	
end

function onLockChanged()
	StateChanged();
end

function updateControl(sControl, bReadOnly)
	if not self[sControl] then
		return false;
	end
		
	return self[sControl].update(bReadOnly);
end

function StateChanged()
	if header.subwindow then
		header.subwindow.update();
	end
	
	if main_weak.subwindow then
		main_weak.subwindow.update();
	end
	if main_tough.subwindow then
		main_tough.subwindow.update();
	end
	if main_steamjack.subwindow then
		main_steamjack.subwindow.update();
	end
	
	
	
	local bReadOnly = WindowManager.getReadOnlyState(getDatabaseNode());
	
	npctype.setReadOnly(bReadOnly);
	text.setReadOnly(bReadOnly);
	
	
end


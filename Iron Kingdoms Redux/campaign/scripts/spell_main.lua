-- 
-- Please see the license.html file included with this distribution for 
-- attribution and copyright information.
--

function onInit()
	update();
end

function VisDataCleared()
	update();
end

function InvisDataAdded()
	update();
end





function updateControl(sControl, bReadOnly, bID)
	if not self[sControl] then
		return false;
	end
		
	if not bID then
		return self[sControl].update(bReadOnly, true);
	end
	
	return self[sControl].update(bReadOnly);
end

function update()
	
	
	local nodeRecord = getDatabaseNode();
	local bReadOnly = WindowManager.getReadOnlyState(getDatabaseNode());
							
	
	updateControl("cost", bReadOnly, true);
	updateControl("range", bReadOnly, true);
	updateControl("aoe", bReadOnly, true);
	updateControl("power", bReadOnly, true);
	updateControl("upkeep", bReadOnly, true);
	updateControl("offensive", bReadOnly, true);
	updateControl("description", bReadOnly, true);
		
	
	
	
	
	
end

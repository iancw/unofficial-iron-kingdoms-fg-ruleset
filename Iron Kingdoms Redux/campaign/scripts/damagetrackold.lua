-- 
-- Please see the license.html file included with this distribution for 
-- attribution and copyright information.
--


local container={ physique={}, agility={}, intellect={} };
local powerfield={};

local sizeBox = { x=13, y=13 };
local sizeBoxSmall={x=20,y=145,w=10,h=10};

local posBox=
{
	
	physique={
	{x=112,y=38},
	{x=98,y=38},	
	{x=84,y=31},	
	{x=84,y=45},
	{x=70,y=31},	
	{x=70,y=45},	
	{x=56,y=31},	
	{x=56,y=45},	
	{x=42,y=31},	
	{x=42,y=45},
	{x=28,y=31},
	{x=28,y=45}
	},
	
	agility={
	{x=112,y=78},
	{x=98,y=78},	
	{x=84,y=71},	
	{x=84,y=85},
	{x=70,y=71},	
	{x=70,y=85},	
	{x=56,y=71},	
	{x=56,y=85},	
	{x=42,y=71},	
	{x=42,y=85},
	{x=28,y=71},
	{x=28,y=85}
	},
	
	intellect={
	{x=112,y=118},
	{x=98,y=118},	
	{x=84,y=111},	
	{x=84,y=125},
	{x=70,y=111},	
	{x=70,y=125},	
	{x=56,y=111},	
	{x=56,y=125},	
	{x=42,y=111},	
	{x=42,y=125},
	{x=28,y=111},
	{x=28,y=125}
	}
	
};




function onInit()
	local sNode = window.getDatabaseNode().getNodeName();
	DB.addHandler(sNode .. ".stats", "onChildUpdate", updateSlots);		
	updateSlots();		
end

function onClose()
	local sNode = window.getDatabaseNode().getNodeName();	
	DB.removeHandler(sNode .. ".stats", "onChildUpdate", updateSlots);	
end












function createTracks(track)
	
	vitality=window[track].getValue();	
	
	
	if (vitality>12) then vitality=12; end;
	
	local track1= math.ceil( math.max(vitality-2,0) / 2);
	local track2= math.max(vitality-2-track1,0);
	
	
	
	
	if #container[track] ~= vitality then
		-- Clear
		for _,v in ipairs(container[track]) do
				v.destroy();
			end
			container[track] = {};
			
		
		for i = 1, vitality do
			
				local leadingzero="";
				if i<10 then leadingzero="0"; end
				
				local control=nil;
				control=window.createControl("damagebox".."_"..track,"damage."..track..".box"..leadingzero..i);										
				control.setAnchor( "top", "damagetrack", "top" ,"absolute",posBox[track][i].y) ;									
				control.setAnchor( "left", "damagetrack", "left" ,"absolute",posBox[track][i].x) ;									
				control.setAnchoredWidth(sizeBox.x);
				control.setAnchoredHeight(sizeBox.y);				
			
				
			container[track][i]=control;
		end
	
	end
	
end






function boxClicked(track)
	local allpressed=true;
	for _,v in ipairs(container[track]) do
		if (v.getValue()==0) then allpressed=false; end;		
	end
	
	
end

function updateSlots()
			
	createTracks("physique");
	createTracks("agility");
	createTracks("intellect");
	
end



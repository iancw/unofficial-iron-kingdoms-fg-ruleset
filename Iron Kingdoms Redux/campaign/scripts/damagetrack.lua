-- 
-- Please see the license.html file included with this distribution for 
-- attribution and copyright information.
--

local sizeBox = { x=13, y=13 };


local posBox=
{
	
	physique={
	{x=112,y=38},
	{x=98,y=38},	
	{x=84,y=31},	
	{x=84,y=45},
	{x=70,y=31},	
	{x=70,y=45},	
	{x=56,y=31},	
	{x=56,y=45},	
	{x=42,y=31},	
	{x=42,y=45},
	{x=28,y=31},
	{x=28,y=45}
	},
	
	agility={
	{x=112,y=78},
	{x=98,y=78},	
	{x=84,y=71},	
	{x=84,y=85},
	{x=70,y=71},	
	{x=70,y=85},	
	{x=56,y=71},	
	{x=56,y=85},	
	{x=42,y=71},	
	{x=42,y=85},
	{x=28,y=71},
	{x=28,y=85}
	},
	
	intellect={
	{x=112,y=118},
	{x=98,y=118},	
	{x=84,y=111},	
	{x=84,y=125},
	{x=70,y=111},	
	{x=70,y=125},	
	{x=56,y=111},	
	{x=56,y=125},	
	{x=42,y=111},	
	{x=42,y=125},
	{x=28,y=111},
	{x=28,y=125}
	}
	
};



local posBox2=
{
	
	[1]={
	{x=112,y=38},
	{x=98,y=38},	
	{x=84,y=31},		
	{x=70,y=31},		
	{x=56,y=31},		
	{x=42,y=31},		
	{x=28,y=31},	
	},
	
	[2]={	
	{x=84,y=45},	
	{x=70,y=45},		
	{x=56,y=45},		
	{x=42,y=45},	
	{x=28,y=45}
	},
	
	
	
	
};



slots={};

damage={};
maxdmg={};

function onInit()
	local sNode = window.getDatabaseNode().getNodeName();
	DB.addHandler(sNode .. ".stats", "onChildUpdate", updateSlots);		

	maxdmg[1]=createTrackCounter("track_1");
	maxdmg[2]=createTrackCounter("track_2");
	maxdmg[3]=createTrackCounter("track_3");
	maxdmg[4]=createTrackCounter("track_4");
	maxdmg[5]=createTrackCounter("track_5");
	maxdmg[6]=createTrackCounter("track_6");
	
	
	damage[1]=createTrackCounter("track_1");
	damage[2]=createTrackCounter("track_2");
	damage[3]=createTrackCounter("track_3");
	damage[4]=createTrackCounter("track_4");
	damage[5]=createTrackCounter("track_5");
	damage[6]=createTrackCounter("track_6");
	
	updateSlots();		
	
	
end

function createTrackCounter(countername)

	local nodeWin = window.getDatabaseNode();
	local nodeCount = nodeWin.getChild("damage."..countername);
	if not nodeCount then
		nodeCount = nodeWin.createChild("damage."..countername, "number");
		nodeCount.onUpdate = update;
	end
	return nodeCount;
	
end

local container={ };

function createTracks(track,trackIdA,trackIdB)
	
	vitality=window[track].getValue();	
	
	
	if (vitality>12) then vitality=12; end;
	
	local trackA= math.ceil( math.max(vitality-2,0) / 2);
	local trackB= math.max(vitality-2-trackA,0);
	local root=vitality-trackA-trackB;
	maxdmg[trackIdA]=trackA+root;
	maxdmg[trackIdB]=trackB;	
	
						
	--if #container ~= vitality then
		-- Clear
		for _,v in pairs(container) do			
			v.ctrl.destroy();
			v={};
		end
		container = {};
			
		
		for i = 1, maxdmg[trackIdA] do
				
				boxesLeftInTrack=maxdmg[trackIdA]-damage[trackIdA].getValue();
				
				local control=nil;																
				control=window.createControl("damagebox","track"..trackIdA.."|box"..i);
                
				if ( boxesLeftInTrack>= i) then
					control.setIcon("box_blue");				
				else
					control.setIcon("box_blue_dark");				
				end
				
				control.setAnchor( "top", "damagetrack", "top" ,"absolute",posBox2[trackIdA][i].y) ;									
				control.setAnchor( "left", "damagetrack", "left" ,"absolute",posBox2[trackIdA][i].x) ;									
				control.setAnchoredWidth(sizeBox.x);
				control.setAnchoredHeight(sizeBox.y);	
				container["track"..trackIdA.."|box"..i]={ctrl=control,track=trackIdA,num=maxdmg[trackIdA]-(i-1) };
		end
		
		
		for i = 1, maxdmg[trackIdB] do
																					
				boxesLeftInTrack=maxdmg[trackIdB]-damage[trackIdB].getValue();
				
				local control=nil;																
				control=window.createControl("damagebox","track"..trackIdB.."|box"..i);
																
				if ( boxesLeftInTrack>= i) then
					control.setIcon("box_blue");				
				else
					control.setIcon("box_blue_dark");				
				end
				
				control.setAnchor( "top", "damagetrack", "top" ,"absolute",posBox2[trackIdB][i].y) ;									
				control.setAnchor( "left", "damagetrack", "left" ,"absolute",posBox2[trackIdB][i].x) ;									
				control.setAnchoredWidth(sizeBox.x);
				control.setAnchoredHeight(sizeBox.y);															
				container["track"..trackIdB.."|box"..i]={ctrl=control,track=trackIdB,num=maxdmg[trackIdB]-(i-1) };
		end
	
	--end
	
end



function boxClickRelease(boxid)
	
		
		
		
		
		print(boxid);
		print(container[boxid].track);
		print(container[boxid].num);
		
		damage[container[boxid].track].setValue( container[boxid].num );
		
		update();
		
		
	
end

function onClose()
	local sNode = window.getDatabaseNode().getNodeName();	
	DB.removeHandler(sNode .. ".stats", "onChildUpdate", updateSlots);	
end


function update()
	updateSlots();
	
	if self.onValueChanged then
		self.onValueChanged();
	end
end



function updateSlots()
			
	createTracks("physique",1,2);
	--createTracks("agility",3,4);
	--createTracks("intellect",5,6);
	
	
	
end


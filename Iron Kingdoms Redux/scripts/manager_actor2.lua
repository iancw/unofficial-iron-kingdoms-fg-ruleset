-- 
-- Please see the license.html file included with this distribution for 
-- attribution and copyright information.
--

function getStatScore(rActor, sStat)
	if not sStat then
		return 0;
	end
	local sActorType, nodeActor = ActorManager.getTypeAndNode(rActor);
	if not nodeActor then
		return 0;
	end
	
	local nStatScore = 0;
	
	
	if sActorType == "pc" then	
		nStatScore = DB.getValue(nodeActor, "stats."..sStat.. ".score", 0);
	elseif sActorType == "npc" then	
		nStatScore = DB.getValue(nodeActor,sStat, 0);					
	end
	
	return nStatScore;
end

function modFeatPoints(rActor,bIncrement)
	local sActorType, nodeActor = ActorManager.getTypeAndNode(rActor);
	if not nodeActor then
		return;
	end	
	
	local sValueName = "";
	if sActorType == "pc" then	
		sValueName = "stats.featpoints";
	elseif sActorType == "npc" then	
		sValueName = "featpoints";			
	end
	
	local nFeatPoints = DB.getValue(nodeActor, sValueName, -1);
	if nFeatPoints == -1 then
		return;
	end
	
	if bIncrement and nFeatPoints < 3  then
		Debug.console("added feat point");
		DB.setValue(nodeActor, sValueName, "number", nFeatPoints+1);
	elseif not bIncrement and nFeatPoints ~= 0 then
		DB.setValue(nodeActor, sValueName, "number", nFeatPoints-1);
	end
end

function getStatString(rActor,sStatName)
	local sStatString = "";
	local sActorType, nodeActor = ActorManager.getTypeAndNode(rActor);
	
	if not nodeActor then
		return sStatString;
	end
	
	if sActorType == "pc" then
		sStatString = DB.getValue(nodeActor, "stats."..sStatName, "cant find "..sStatName);
	elseif sActorType == "npc" then	
		sStatString = DB.getValue(nodeActor,"stats."..sStatName, "cant find "..sStatName);					
	end
	
	--Debug.chat(sRaceName);
	
	return sStatString;
end

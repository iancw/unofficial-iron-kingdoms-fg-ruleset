-- 
-- Please see the license.html file included with this distribution for 
-- attribution and copyright information.
--

function onInit()
	Debug.console("skill init");
	ActionsManager.registerModHandler("skill", modRoll);
	ActionsManager.registerResultHandler("skill", onRoll);
end

function getRoll(rActor, sSkillName, nSkillMod, sStatName, nTargetDC, bSecretRoll, sExtra)
	Debug.console("skill getRoll");
	local rRoll = {};
	rRoll.sType = "skill";
	rRoll.aDice = { "d6","d6" };
	
	rRoll.nMod = nSkillMod or 0;
	rRoll.sDesc = "[SKILL] " .. sSkillName;
	if sExtra then
		rRoll.sDesc = rRoll.sDesc .. " " .. sExtra;
	end
	
	Utility.checkNyssPerception(rRoll,rActor,sStatName);	--check nyss special rule

	rRoll.bSecret = bSecretRoll;

	rRoll.nTarget = nTargetDC;
	
	return rRoll;
end

function performRoll(draginfo, rActor, sSkillName, nSkillTotal, sStatName)					
	local rRoll = getRoll(rActor, sSkillName,nSkillTotal, sStatName, nTargetDC, bSecretRoll);
	ActionsManager.performAction(draginfo, rActor, rRoll);
end

function modRoll(rSource, rTarget, rRoll)
	Debug.console("skill modRoll");
	Utility.modRoll(rSource,rTarget,rRoll);
end

function onRoll(rSource, rTarget, rRoll)
	Debug.console("skill onRoll");
	local rMessage = ActionsManager.createActionMessage(rSource, rRoll);
	
	Utility.checkVirtuoso(rRoll);	--check for drop lowest die
	Utility.checkCritical(rRoll,rMessage,rSource);	--check for crit
		
	if rRoll.nTarget then
		local nTotal = ActionsManager.total(rRoll);
		local nTargetDC = tonumber(rRoll.nTarget) or 0;
		
		rMessage.text = rMessage.text .. " (vs. DC " .. nTargetDC .. ")";
		if nTotal >= nTargetDC then
			rMessage.text = rMessage.text .. " [SUCCESS]";
		else
			rMessage.text = rMessage.text .. " [FAILURE]";
		end
	end
	
	Comm.deliverChatMessage(rMessage);
end


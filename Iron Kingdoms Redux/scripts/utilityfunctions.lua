function trim(s)
  return s:match'^()%s*$' and '' or s:match'^%s*(.*%S)'
end

function parseNodeName(s)
	return string.lower(s:gsub('%W',''))
end

function contains(typeString, testString)
	local lowerTypeString = string.lower(typeString);
	local lowerTestString = string.lower(testString);
	
	if string.find(lowerTypeString,lowerTestString) then
		return true;
	else
		return false;
	end
	
end

function modRoll(rSource, rTarget, rRoll,nAddMod)
	local aAddDesc = {};
	nAddMod = nAddMod or 0;
	
	local bAddDrop=Input.isAltPressed();
	local bAddDice=Input.isShiftPressed();
	
	if bAddDrop then
		rRoll.sDesc = rRoll.sDesc .. "\r[VIRTUOSO]";
		table.insert(rRoll.aDice, "d6");
	end
	if bAddDice then
		rRoll.sDesc = rRoll.sDesc .. "\r[BOOSTED]";
		table.insert(rRoll.aDice, "d6");
	end
	rRoll.nMod = rRoll.nMod + nAddMod;
end

function checkVirtuoso(rRoll) 
	--Debug.console("util");
	if string.match(rRoll.sDesc,"%[VIRTUOSO%]") then	
	
		lowestRollIndex=0;
		lowestRoll=1000;
		if rRoll.aDice~=nil then
			for i, roll in ipairs(rRoll.aDice) do
				if roll.result < lowestRoll then
					lowestRoll=roll.result;
					lowestRollIndex=i;
				end
			end
		end
		
		rRoll.aDice[lowestRollIndex]=nil;
	end
end

function checkCritical(rRoll,rMessage,rActor,bIsAttack) 
	--Debug.console("checkCritical called");
	local matches={
		[1]=0,
		[2]=0,
		[3]=0,
		[4]=0,
		[5]=0,
		[6]=0,
	};
	
	local sCritHitVerb = "";
	local sCritMissVerb = "";
	bIsAttack = bIsAttack or false;
	if bIsAttack then
		sCritHitVerb = "HIT";
		sCritMissVerb = "MISS";
	else
		sCritHitVerb = "SUCCESS";
		sCritMissVerb = "FAIL";
	end
	
	local bIsCrit = false;
	local bGainFeat = false;
	local nDieCount = 0;
	
	for i, die in pairs (rRoll.aDice) do
		nDieCount = nDieCount + 1;
		matches[die.result]=matches[die.result]+1;
	end
	
	for i,nummatch in ipairs(matches) do
		if nummatch > 1 then bIsCrit=true; end		
	end
	
	if bIsCrit then
		bGainFeat = true;
		-- check for critical failure/success
		if matches[1] == nDieCount then
			rMessage.text=rMessage.text.."\r[CRITICAL " .. sCritMissVerb .. "]";
			bGainFeat = false;
		elseif matches[6] == nDieCount then
			rMessage.text=rMessage.text.."\r[CRITICAL]";
			rMessage.text=rMessage.text.."\r[AUTO-" .. sCritHitVerb .. "]";
		else
			rMessage.text=rMessage.text.."\r[CRITICAL]";
		end
		-- give feat points
		bGainFeat = false; -- disabling this for now
		if rActor~=nil and bGainFeat then
			--Debug.console("got rActor, using feat point: "..bGainFeat);
			ActorManager2.modFeatPoints(rActor,true); -- increment feat points for rActor
		end
	end
end

function checkNyssPerception(rRoll,rActor,sStatName) 
	if Utility.contains(sStatName,"per") and Utility.contains(ActorManager2.getStatString(rActor,"race"),"nyss") then
		rRoll.sDesc = rRoll.sDesc .. "\r+[NYSS] 1";
		rRoll.nMod = rRoll.nMod + 1;
	end
end


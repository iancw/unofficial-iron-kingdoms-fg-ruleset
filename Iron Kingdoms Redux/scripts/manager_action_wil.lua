-- 
-- Please see the license.html file included with this distribution for 
-- attribution and copyright information.
--

function onInit()	
	Debug.console("wil init");
	ActionsManager.registerModHandler("wil", modRoll);
	ActionsManager.registerResultHandler("wil", onResolve);
end

function getRoll(rActor, bSecretRoll)
	Debug.console("wil getRoll");
	local rRoll = {};
	rRoll.sType = "wil";
	rRoll.aDice = { "d6","d6" };
	rRoll.nMod = 0;
	rRoll.sDesc = "[WILLPOWER]";
	rRoll.bSecret = bSecretRoll;
	
	local sActorType, nodeActor = ActorManager.getTypeAndNode(rActor);
	
	if(sActorType=="pc") then
		rRoll.nMod = DB.getValue(nodeActor, "derived.willpower.total", 0);
	elseif(sActorType=="npc" or sActorType=="ct") then
		rRoll.nMod = DB.getValue(nodeActor, "willpower", 0);		
	end
	
	return rRoll;
end

function performRoll(draginfo, rActor, bSecretRoll)
	local rRoll = getRoll(rActor, bSecretRoll);
	ActionsManager.performAction(draginfo, rActor, rRoll);
end

function modRoll(rSource, rTarget, rRoll)
	Debug.console("wil modRoll");
	Utility.modRoll(rSource,rTarget,rRoll);
end

function onResolve(rSource, rTarget, rRoll)
	Debug.console("wil onResolve");
	local rMessage = ActionsManager.createActionMessage(rSource, rRoll);
	
	-- these cannot be performed on willpower?
	--Utility.checkVirtuoso(rRoll);	--check for drop lowest die
	--Utility.checkCritical(rRoll,rMessage);	--check for crit
		
	if rRoll.nTarget then
		local nTotal = ActionsManager.total(rRoll);
		local nTargetDC = tonumber(rRoll.nTarget) or 0;
		
		rMessage.text = rMessage.text .. " (vs. DC " .. nTargetDC .. ")";
		if nTotal >= nTargetDC then
			rMessage.text = rMessage.text .. " [SUCCESS]";
		else
			rMessage.text = rMessage.text .. " [FAILURE]";
		end
	end
	
	Comm.deliverChatMessage(rMessage);
end

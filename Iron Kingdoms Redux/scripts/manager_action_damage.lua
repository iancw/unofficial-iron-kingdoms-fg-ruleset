-- 
-- Please see the license.html file included with this distribution for 
-- attribution and copyright information.
--

OOB_MSGTYPE_APPLYDMG = "applydmg";

function onInit()
	--OOBManager.registerOOBMsgHandler(OOB_MSGTYPE_APPLYDMG, handleApplyDamage);

	ActionsManager.registerModHandler("damage", modDamage);	
	ActionsManager.registerResultHandler("damage", onDamage);
end

function handleApplyDamage(msgOOB)
	local rSource = ActorManager.getActor(msgOOB.sSourceType, msgOOB.sSourceNode);
	local rTarget = ActorManager.getActor(msgOOB.sTargetType, msgOOB.sTargetNode);
	if rTarget then
		rTarget.nOrder = msgOOB.nTargetOrder;
	end
	
	local nTotal = tonumber(msgOOB.nTotal) or 0;
	applyDamage(rSource, rTarget, (tonumber(msgOOB.nSecret) == 1), msgOOB.sRollType, msgOOB.sDamage, nTotal);
end

function notifyApplyDamage(rSource, rTarget, bSecret, sRollType, sDesc, nTotal)
	if not rTarget then
		return;
	end
	local sTargetType, sTargetNode = ActorManager.getTypeAndNodeName(rTarget);
	if sTargetType ~= "pc" and sTargetType ~= "ct" then
		return;
	end

	local msgOOB = {};
	msgOOB.type = OOB_MSGTYPE_APPLYDMG;
	
	if bSecret then
		msgOOB.nSecret = 1;
	else
		msgOOB.nSecret = 0;
	end
	msgOOB.sRollType = sRollType;
	msgOOB.nTotal = nTotal;
	msgOOB.sDamage = sDesc;
	msgOOB.sTargetType = sTargetType;
	msgOOB.sTargetNode = sTargetNode;
	msgOOB.nTargetOrder = rTarget.nOrder;

	local sSourceType, sSourceNode = ActorManager.getTypeAndNodeName(rSource);
	msgOOB.sSourceType = sSourceType;
	msgOOB.sSourceNode = sSourceNode;

	Comm.deliverOOBMessage(msgOOB, "");
end

function getRoll(rActor, weaponPower,applyStr,str)
	Debug.console("damage getRoll");
	local rRoll = {};
	rRoll.sType = "damage";
	rRoll.aDice = {"d6","d6"};
	
	rRoll.nMod = weaponPower + applyStr*str;
	rRoll.sDesc = "[DAMAGE]"
	if(applyStr==1) then
		rRoll.sDesc = rRoll.sDesc.." +STR "..str;
	end
	
	--check for Mighty archetype (add dice on all damage rolls)
	local sArchetype = ActorManager2.getStatString(rActor,"archetype");
	if Utility.contains(sArchetype,"might") and applyStr==1 then
		rRoll.sDesc = rRoll.sDesc .. "\r[MIGHTY]";
		table.insert(rRoll.aDice, "d6");
	end
	--check for intellectual/cunning (+1 dmg)
	local sArchetype = ActorManager2.getStatString(rActor,"archetype");
	if Utility.contains(sArchetype,"intel") then
		rRoll.sDesc = rRoll.sDesc .. "\r+[INTEL.] 1";
		rRoll.nMod = rRoll.nMod + 1;
	elseif Utility.contains(sArchetype,"cun") then
		rRoll.sDesc = rRoll.sDesc .. "\r+[CUNNING] 1";
		rRoll.nMod = rRoll.nMod + 1;
	end
		
	return rRoll;
end

function performRoll(draginfo, rActor, nodeWeapon)
	weaponPower=DB.getValue(nodeWeapon, "power", 0);
	applyStr =DB.getValue(nodeWeapon, "powerapplystr", 0);
	str=ActorManager2.getStatScore(rActor,"strength");
	
	local rRoll = getRoll(rActor, weaponPower, applyStr, str);
	
	ActionsManager.performAction(draginfo, rActor, rRoll, true);
end

function modDamage(rSource, rTarget, rRoll)
	Debug.console("damage modDamage");
	Utility.modRoll(rSource,rTarget,rRoll);
end

function onDamage(rSource, rTarget, rRoll)
	Debug.console("damage onDamage");
	local rMessage = ActionsManager.createActionMessage(rSource, rRoll);

	local bIsSourcePC = (rSource and rSource.sType == "pc");
				
	local rAction = {};
	rAction.nTotal = ActionsManager.total(rRoll);
	rAction.aMessages = {};
	
	Utility.checkVirtuoso(rRoll); --check for drop lowest die
	Utility.checkCritical(rRoll,rMessage);	--check for crit
	
	Comm.deliverChatMessage(rMessage);
end


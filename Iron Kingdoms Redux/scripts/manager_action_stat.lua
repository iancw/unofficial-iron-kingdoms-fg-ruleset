-- 
-- Please see the license.html file included with this distribution for 
-- attribution and copyright information.
--

function onInit()
	Debug.console("stat init");
	ActionsManager.registerModHandler("stat", modRoll);
	ActionsManager.registerResultHandler("stat", onRoll);
end

function getRoll(rActor, sAbilityStat, nTargetDC, bSecretRoll)
	Debug.console("stat getroll");
	local rRoll = {};
	rRoll.sType = "stat";
	rRoll.aDice = { "d6","d6" };
	
	rRoll.nMod = ActorManager2.getStatScore(rActor, Utility.parseNodeName(sAbilityStat));
		
	rRoll.sDesc = "[STAT]";
	rRoll.sDesc = rRoll.sDesc .. " " .. StringManager.capitalize(sAbilityStat);

	rRoll.bSecret = bSecretRoll;

	rRoll.nTarget = nTargetDC;
	
	Utility.checkNyssPerception(rRoll,rActor,sAbilityStat);	--check nyss special rule
	
	return rRoll;
end

function performRoll(draginfo, rActor, sAbilityStat, nTargetDC, bSecretRoll)
	local rRoll = getRoll(rActor, sAbilityStat, nTargetDC, bSecretRoll);
	ActionsManager.performAction(draginfo, rActor, rRoll);
end

function modRoll(rSource, rTarget, rRoll)	
	Debug.console("stat modRoll");
	Utility.modRoll(rSource,rTarget,rRoll);
end

function onRoll(rSource, rTarget, rRoll)
	Debug.console("stat onRoll");
	local rMessage = ActionsManager.createActionMessage(rSource, rRoll);
	
	rRoll.sDesc = rRoll.sDesc .. "total: " .. ActionsManager.total(rRoll);
	Utility.checkVirtuoso(rRoll);	--check for drop lowest die
	Utility.checkCritical(rRoll,rMessage,rSource);	--check for crit
		
	if rRoll.nTarget then
		local nTotal = ActionsManager.total(rRoll);
		local nTargetDC = tonumber(rRoll.nTarget) or 0;
		
		rMessage.text = rMessage.text .. " (vs. DC " .. nTargetDC .. ")";
		if nTotal >= nTargetDC then
			rMessage.text = rMessage.text .. " [SUCCESS]";
		else
			rMessage.text = rMessage.text .. " [FAILURE]";
		end
	end
	
	Comm.deliverChatMessage(rMessage);
end


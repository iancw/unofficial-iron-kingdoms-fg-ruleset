-- 
-- Please see the license.html file included with this distribution for 
-- attribution and copyright information.
--

OOB_MSGTYPE_APPLYINIT = "applyinit";

function onInit()
	Debug.console("init onInit");
	OOBManager.registerOOBMsgHandler(OOB_MSGTYPE_APPLYINIT, handleApplyInit);
	ActionsManager.registerModHandler("init", modRoll);
	ActionsManager.registerResultHandler("init", onResolve);
end

function handleApplyInit(msgOOB)
	local rSource = ActorManager.getActor(msgOOB.sSourceType, msgOOB.sSourceNode);
	local nTotal = tonumber(msgOOB.nTotal) or 0;

	DB.setValue(ActorManager.getCTNode(rSource), "initresult", "number", nTotal);
end

function notifyApplyInit(rSource, nTotal)
	if not rSource then
		return;
	end
	
	
	local msgOOB = {};
	msgOOB.type = OOB_MSGTYPE_APPLYINIT;
	
	msgOOB.nTotal = nTotal;

	local sSourceType, sSourceNode = ActorManager.getTypeAndNodeName(rSource);
	msgOOB.sSourceType = sSourceType;
	msgOOB.sSourceNode = sSourceNode;

	Comm.deliverOOBMessage(msgOOB, "");
end

function getRoll(rActor, bSecretRoll)
	Debug.console("init getRoll");
	local rRoll = {};
	rRoll.sType = "init";
	rRoll.aDice = { "d6","d6" };
	rRoll.nMod = 0;
	rRoll.sDesc = "[INIT]";
	rRoll.bSecret = bSecretRoll;
	
	local sActorType, nodeActor = ActorManager.getTypeAndNode(rActor);
	
	if(sActorType=="pc") then
		rRoll.nMod = DB.getValue(nodeActor, "derived.initiative.total", 0);
	elseif(sActorType=="npc" or sActorType=="ct") then
		rRoll.nMod = DB.getValue(nodeActor, "initiative", 0);		
	end
	
	return rRoll;
end

function performRoll(draginfo, rActor, bSecretRoll)
	local rRoll = getRoll(rActor, bSecretRoll);	
	ActionsManager.performAction(draginfo, rActor, rRoll);
end

function modRoll(rSource, rTarget, rRoll)
	Debug.console("init modRoll");
	Utility.modRoll(rSource,rTarget,rRoll);
end

function onResolve(rSource, rTarget, rRoll)
	Debug.console("init onResolve");
	Utility.checkVirtuoso(rRoll);
	
	local rMessage = ActionsManager.createActionMessage(rSource, rRoll);
	Comm.deliverChatMessage(rMessage);
	
	local nTotal = ActionsManager.total(rRoll);
	notifyApplyInit(rSource, nTotal);
end

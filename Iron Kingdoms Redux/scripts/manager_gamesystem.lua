-- 
-- Please see the license.html file included with this distribution for 
-- attribution and copyright information.
--

-- Ruleset action types
actions = {
	["dice"] = { bUseModStack = true },
	["table"] = { },
	["effect"] = { sIcon = "action_effect", sTargeting = "all" },
	["attack"] = { sIcon = "action_attack", sTargeting = "each", bUseModStack = true },	
	["magicattack"] = { sIcon = "action_attack", sTargeting = "each", bUseModStack = true },	
	["damage"] = { sIcon = "action_damage", sTargeting = "each", bUseModStack = true },
	["skill"] = { bUseModStack = true },
	["init"] = { bUseModStack = true },
	["stat"] = { bUseModStack = true },
	["wil"] = { bUseModStack = true }
};

targetactions = {
	"attack",
	"damage"	
};

currencies = { "PP", "GP", "SP", "CP" };


function getCharSelectDetailHost(nodeChar)		
	local sLevel = DB.getValue(nodeChar, "level","Heroic");		
	return sLevel;	
end

function requestCharSelectDetailClient()	
	return "name,level";
end

function receiveCharSelectDetailClient(vDetails)	
	return vDetails[1], "Level " .. vDetails[2];
end

function getCharSelectDetailLocal(nodeLocal)	
	local vDetails = {};
	table.insert(vDetails, DB.getValue(nodeLocal, "name", ""));
	table.insert(vDetails, DB.getValue(nodeLocal, "level","Heroic"));	
	return receiveCharSelectDetailClient(vDetails);
end

function getDistanceUnitsPerGrid()
	return 1;
end


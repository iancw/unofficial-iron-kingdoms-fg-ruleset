-- 
-- Please see the license.html file included with this distribution for 
-- attribution and copyright information.
--

OOB_MSGTYPE_APPLYATK = "applyatk";


function onInit()
	Debug.console("attack init");
	OOBManager.registerOOBMsgHandler(OOB_MSGTYPE_APPLYATK, handleApplyAttack);
	ActionsManager.registerTargetingHandler("attack", onTargeting);
	ActionsManager.registerModHandler("attack", modAttack);
	ActionsManager.registerResultHandler("attack", onAttack);
end

function handleApplyAttack(msgOOB)
	local rSource = ActorManager.getActor(msgOOB.sSourceType, msgOOB.sSourceNode);
	local rTarget = ActorManager.getActor(msgOOB.sTargetType, msgOOB.sTargetNode);
	
	local nTotal = tonumber(msgOOB.nTotal) or 0;
	applyAttack(rSource, rTarget, (tonumber(msgOOB.nSecret) == 1), msgOOB.sAttackType, msgOOB.sDesc, nTotal, msgOOB.sResults);
end

function notifyApplyAttack(rSource, rTarget, bSecret, sAttackType, sDesc, nTotal, sResults)
	if not rTarget then
		return;
	end

	local msgOOB = {};
	msgOOB.type = OOB_MSGTYPE_APPLYATK;
	
	if bSecret then
		msgOOB.nSecret = 1;
	else
		msgOOB.nSecret = 0;
	end
	msgOOB.sAttackType = sAttackType;
	msgOOB.nTotal = nTotal;
	msgOOB.sDesc = sDesc;
	msgOOB.sResults = sResults;

	local sSourceType, sSourceNode = ActorManager.getTypeAndNodeName(rSource);
	msgOOB.sSourceType = sSourceType;
	msgOOB.sSourceNode = sSourceNode;

	local sTargetType, sTargetNode = ActorManager.getTypeAndNodeName(rTarget);
	msgOOB.sTargetType = sTargetType;
	msgOOB.sTargetNode = sTargetNode;

	Comm.deliverOOBMessage(msgOOB, "");
end

function onTargeting(rSource, aTargeting, rRolls)
	if OptionsManager.isOption("RMMT", "multi") then
		local aTargets = {};
		for _,vTargetGroup in ipairs(aTargeting) do
			for _,vTarget in ipairs(vTargetGroup) do
				table.insert(aTargets, vTarget);
			end
		end
		if #aTargets > 1 then
			for _,vRoll in ipairs(rRolls) do
				if not string.match(vRoll.sDesc, "%[FULL%]") then
					vRoll.bRemoveOnMiss = "true";
				end
			end
		end
	end
	return aTargeting;
end

function getRoll(rActor, weaponwindow,skillwindow)
	local rRoll = {};
	rRoll.sDesc= "\r"..weaponwindow.name.getValue();
	rRoll.sType = "attack";
	rRoll.aDice = { "d6","d6" };
	
	rRoll.nMod = weaponwindow.attack.getValue() or 0;
	if rRoll.nMod~=0 then
		rRoll.sDesc = rRoll.sDesc.."\r+[WEAPON] "..weaponwindow.attack.getValue();
	end
	
	if (skillwindow~=nil) then
		rRoll.nMod = rRoll.nMod + skillwindow.total.getValue();
		rRoll.sDesc=rRoll.sDesc.."\r+[SKILL] "..skillwindow.total.getValue();
	else
		--Debug.chat(weaponwindow.type.getValue());
		if Utility.contains(weaponwindow.type.getValue(),"Ranged") then
			rRoll.nMod =rRoll.nMod + ActorManager2.getStatScore(rActor, "poise");
			rRoll.sDesc=rRoll.sDesc.."\r+[STAT] Poise "..ActorManager2.getStatScore(rActor, "poise");
		else
			rRoll.nMod = rRoll.nMod + ActorManager2.getStatScore(rActor, "prowess");
			rRoll.sDesc=rRoll.sDesc.."\r+[STAT] Prow. "..ActorManager2.getStatScore(rActor, "prowess");
		end
		
	end
	
	--check for intellectual/cunning (+1 to hit)
	local sArchetype = ActorManager2.getStatString(rActor,"archetype");
	if Utility.contains(sArchetype,"intel") then
		rRoll.sDesc = rRoll.sDesc .. "\r+[INTEL.] 1";
		rRoll.nMod = rRoll.nMod + 1;
	elseif Utility.contains(sArchetype,"cun") then
		rRoll.sDesc = rRoll.sDesc .. "\r+[CUNNING] 1";
		rRoll.nMod = rRoll.nMod + 1;
	end
	
	return rRoll;
end

function performRoll(draginfo, rActor,weaponwindow, skillwindow)
	local rRoll = getRoll(rActor, weaponwindow, skillwindow);
	--Debug.console(skillwindow);
	--Debug.console("attack performRoll");
	ActionsManager.performAction(draginfo, rActor, rRoll);
	
end

function modAttack(rSource, rTarget, rRoll)
	Debug.console("attack modAttack");
	Utility.modRoll(rSource,rTarget,rRoll);
end

function onAttack(rSource, rTarget, rRoll)
	Debug.console("attack onAttack");
	local rMessage = ActionsManager.createActionMessage(rSource, rRoll);

	local bIsSourcePC = (rSource and rSource.sType == "pc");
				
	local rAction = {};
	rAction.nTotal = ActionsManager.total(rRoll);
	rAction.aMessages = {};	
	
	Utility.checkVirtuoso(rRoll);	--check for drop lowest die
	Utility.checkCritical(rRoll,rMessage,rSource,true);	--check for crit

	if rTarget then
		notifyApplyAttack(rSource, rTarget, rMessage.secret, rRoll.sType, rRoll.sDesc, rAction.nTotal, table.concat(rAction.aMessages, " "));
		
		-- REMOVE TARGET ON MISS OPTION
		if (rAction.sResult == "miss") then
			local bRemoveTarget = false;
			if OptionsManager.isOption("RMMT", "on") then
				bRemoveTarget = true;
			elseif rRoll.bRemoveOnMiss then
				bRemoveTarget = true;
			end
			
			if bRemoveTarget then
				TargetingManager.removeTarget(ActorManager.getCTNodeName(rSource), ActorManager.getCTNodeName(rTarget));
			end
		end
	end
	
	Comm.deliverChatMessage(rMessage);
end


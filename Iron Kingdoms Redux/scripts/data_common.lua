-- 
-- Please see the license.html file included with this distribution for 
-- attribution and copyright information.
--

launchmsg = "Iron Kingdoms RPG v0.0.1 ruleset for Fantasy Grounds.";



stat_ltos = {
	["physique"] = "PHY",
	["speed"] = "SPD",
	["strength"] = "STR",
	["agility"] = "AGI",
	["prowess"] = "PRW",
	["poise"] = "POI",
	["intellect"] = "INT",
	["arcane"] = "ARC",
	["perception"] = "PER"
};

stat_stol = {
	["PHY"] = "physique",
	["SPD"] = "speed",
	["STR"] = "strength",
	["AGI"] = "agility",
	["PRW"] = "prowess",
	["POI"] = "poise",
	["INT"] = "intellect",
	["ARC"] = "arcane",
	["PER"] = "perception"
};

skilldata = {
	"Archery","Crossbow","Great Weapon","Hand Weapon","Lance","Light Artillery",
	"Pistol","Rifle","Shield","Thrown Weapon","Unarmed Combat",
	"Alchemy","Bribery","Command","Craft","Cryptography","Deception","Disguise",
	"Escape Artist","Etiquette","Fell Calling","Forensic Science","Forgery",
	"Interrogation","Law","Lock Picking","Mechanikal Eng.","Medicine","Navigation",
	"Negotiation","Oratory","Pickpocket","Research","Rope Use","Sailing","Seduction",
	"Sneak","Streetwise","Survival","Tracking",
	"Animal Handling","Climbing","Detection","Driving","Gambling","Intimidation",
	"Jumping","Lore","Riding","Swimming"
}


connectors = {
	"and",
	"or"
};

-- Range types supported
rangetypes = {
	"melee",
	"ranged"
};

weaponskills = {
	"Archery","Crossbow","Great Weapon","Hand Weapon","Lance","Light Artillery",
	"Pistol","Rifle","Shield","Thrown Weapon","Unarmed Combat"
}

-- Coin labels
currency = { "gc","sc","cc" };



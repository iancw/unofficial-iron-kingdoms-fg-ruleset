-- 
-- Please see the license.html file included with this distribution for 
-- attribution and copyright information.
--

OOB_MSGTYPE_APPLYATK = "applyatk";

function onInit()
	Debug.console("magicattack init");
	OOBManager.registerOOBMsgHandler(OOB_MSGTYPE_APPLYATK, handleApplyAttack);
	ActionsManager.registerTargetingHandler("magicattack", onTargeting);
	ActionsManager.registerModHandler("magicattack", modAttack);
	ActionsManager.registerResultHandler("magicattack", onAttack);

end

function handleApplyAttack(msgOOB)
	local rSource = ActorManager.getActor(msgOOB.sSourceType, msgOOB.sSourceNode);
	local rTarget = ActorManager.getActor(msgOOB.sTargetType, msgOOB.sTargetNode);
	
	local nTotal = tonumber(msgOOB.nTotal) or 0;
	applyAttack(rSource, rTarget, (tonumber(msgOOB.nSecret) == 1), msgOOB.sAttackType, msgOOB.sDesc, nTotal, msgOOB.sResults);
end

function notifyApplyAttack(rSource, rTarget, bSecret, sAttackType, sDesc, nTotal, sResults)
	if not rTarget then
		return;
	end

	local msgOOB = {};
	msgOOB.type = OOB_MSGTYPE_APPLYATK;
	
	if bSecret then
		msgOOB.nSecret = 1;
	else
		msgOOB.nSecret = 0;
	end
	msgOOB.sAttackType = sAttackType;
	msgOOB.nTotal = nTotal;
	msgOOB.sDesc = sDesc;
	msgOOB.sResults = sResults;

	local sSourceType, sSourceNode = ActorManager.getTypeAndNodeName(rSource);
	msgOOB.sSourceType = sSourceType;
	msgOOB.sSourceNode = sSourceNode;

	local sTargetType, sTargetNode = ActorManager.getTypeAndNodeName(rTarget);
	msgOOB.sTargetType = sTargetType;
	msgOOB.sTargetNode = sTargetNode;

	Comm.deliverOOBMessage(msgOOB, "");
end


function onTargeting(rSource, aTargeting, rRolls)
	if OptionsManager.isOption("RMMT", "multi") then
		local aTargets = {};
		for _,vTargetGroup in ipairs(aTargeting) do
			for _,vTarget in ipairs(vTargetGroup) do
				table.insert(aTargets, vTarget);
			end
		end
		if #aTargets > 1 then
			for _,vRoll in ipairs(rRolls) do
				if not string.match(vRoll.sDesc, "%[FULL%]") then
					vRoll.bRemoveOnMiss = "true";
				end
			end
		end
	end
	return aTargeting;
end

function getRoll(rActor, magicattack)
	Debug.console("magicattack getRoll");
	local rRoll = {};
	
	rRoll.sType = "magicattack";

	rRoll.aDice = { "d6","d6" };
	rRoll.nMod = magicattack or 0;
	rRoll.sDesc = "[MAGIC ATTACK]"
	
	
	return rRoll;
end

function performRoll(draginfo, rActor,magicattack)
	local rRoll = getRoll(rActor, magicattack);
	
	--Debug.chat(rActor);
	ActionsManager.performAction(draginfo, rActor, rRoll);
	
end

function modAttack(rSource, rTarget, rRoll)
	Debug.console("magicattack modAttack");
	Utility.modRoll(rSource,rTarget,rRoll);
end

function onAttack(rSource, rTarget, rRoll)
	Debug.console("magicattack onAttack");
	local rMessage = ActionsManager.createActionMessage(rSource, rRoll);

	local bIsSourcePC = (rSource and rSource.sType == "pc");
				
	local rAction = {};
	rAction.nTotal = ActionsManager.total(rRoll);
	rAction.aMessages = {};
	
	Utility.checkVirtuoso(rRoll); 	--check for drop lowest die
	Utility.checkCritical(rRoll,rMessage,rSource);	--check for crit	

	if rTarget then
		notifyApplyAttack(rSource, rTarget, rMessage.secret, rRoll.sType, rRoll.sDesc, rAction.nTotal, table.concat(rAction.aMessages, " "));
		
		-- REMOVE TARGET ON MISS OPTION
		if (rAction.sResult == "miss") then
			local bRemoveTarget = false;
			if OptionsManager.isOption("RMMT", "on") then
				bRemoveTarget = true;
			elseif rRoll.bRemoveOnMiss then
				bRemoveTarget = true;
			end
			
			if bRemoveTarget then
				TargetingManager.removeTarget(ActorManager.getCTNodeName(rSource), ActorManager.getCTNodeName(rTarget));
			end
		end
	end
	
	Comm.deliverChatMessage(rMessage);
	
end


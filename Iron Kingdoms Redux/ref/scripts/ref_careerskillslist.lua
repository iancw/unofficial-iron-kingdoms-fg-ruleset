function onInit()
					
	local skilltype=skilltype[1];
	--Debug.chat(skilltype);
	
	local node=window.getDatabaseNode().getChild(skilltype);
	
	local parsestring="";
	if node.getChild("parsestring") then
		parsestring=node.getChild("parsestring").getValue();
	end
	
	
		
		local labels =StringManager.split(parsestring, ",") ;
		
		local nStarts, nEnds, sSkill, sMax, sSpec;
		
		for i,skillname in pairs(labels) do
			local win=createWindow();
			
			
			
			nStarts, nEnds, sSkill, sMax, sSpec = string.find(Utility.trim(skillname) , "([%w%s]*)%s(%d+)%s?([%w%s/(/)]*)");
			
			
			if sMax then
				win.max.setValue(sMax);
			else
				win.max.setVisible(false);
			end
			
			--Debug.chat(trim(skillname));			
			--Debug.chat(sSkill);
			--Debug.chat(sMax);
			--if sSpec~="" then Debug.chat(sSpec); end
			
			
			
			if sSkill==nil then
					win.name.setValue( "None")
					win.listlink.setVisible(false);
			elseif string.lower(Utility.trim(sSkill))=="general skills" then
					win.name.setValue( Utility.trim(sSkill).." "..sSpec )
					local link = "reference.skills@Iron Kingdoms Core Rules";
					win.listlink.setValue("reference_skilltablelist",link);
			
			
			
				
				
			else
				
				if skilltype=="militaryskills" then
					
					win.name.setValue( Utility.trim(sSkill).." "..sSpec )
					
					local modules=Module.getModules( );						
			
					local link="";
					local bFoundSkill=false;
					for k,target in pairs(modules) do			
						link = "reference.skills.groups.military.index."..Utility.parseNodeName(sSkill).."@"..target;
						targetNode= DB.findNode(link);
						if targetNode then
							win.listlink.setValue("referenceskill",link);
							bFoundSkill=true;
						end
					end
					
					if not bFoundSkill then
						Debug.chat(sSkill);
					end
					
					--win.name.setValue( Utility.trim(sSkill).." "..sSpec )
					--local link = "reference.skills.groups.military.index."..Utility.parseNodeName(sSkill)..moduletarget;
					--win.listlink.setValue("referenceskill",link);
					
				end
				
				if skilltype=="occupationalskills" then
					
					win.name.setValue( Utility.trim(sSkill).." "..sSpec )
					
					local modules=Module.getModules( );						
			
					local link="";
					local bFoundSkill=false;
					for k,target in pairs(modules) do			
						link = "reference.skills.groups.occupational.index."..Utility.parseNodeName(sSkill).."@"..target;
						targetNode= DB.findNode(link);
						if targetNode then
							win.listlink.setValue("referenceskill",link);
							bFoundSkill=true;
						end
					end
					
					if not bFoundSkill then
						Debug.chat(sSkill);
					end
					
					
					--win.name.setValue( Utility.trim(sSkill).." "..sSpec )
					--local link = "reference.skills.groups.occupational.index."..Utility.parseNodeName(sSkill)..moduletarget;
					--win.listlink.setValue("referenceskill",link);
				
				--end
			
			
				
			end
			
			
			
			
		end
		
	


	end


end


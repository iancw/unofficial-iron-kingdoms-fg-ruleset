-- 
-- Please see the license.html file included with this distribution for 
-- attribution and copyright information.
--

function onInit()
	
		if window.spells.getValue()=="" then
			setDatabaseNode( DB.findNode(window.moduletarget.getValue() ) );		
		else
		
			local labels =StringManager.split(window.spells.getValue(), ",") ;
			--Debug.chat(labels);
			
			for i,spellname in pairs(labels) do
				local win=createWindow();
				
				win.name.setValue( Utility.trim(spellname) )
				
				local modules=Module.getModules( );						
				
				local link="";
				local bFoundSpell=false;
				for k,target in pairs(modules) do			
					link = "reference.spells.index."..Utility.parseNodeName(spellname).."@"..target;
					targetNode= DB.findNode(link);
					if targetNode then
						win.shortcut.setValue("spell",link);
						bFoundSpell=true;
						
						local sourceNode=DB.findNode(link);		
						win.cost.setValue(sourceNode.getChild("cost").getValue());
						win.range.setValue(sourceNode.getChild("range").getValue());
						win.aoe.setValue(sourceNode.getChild("aoe").getValue());
						win.power.setValue(sourceNode.getChild("power").getValue());
						win.upkeep.setValue(sourceNode.getChild("upkeep").getValue());
						win.offensive.setValue(sourceNode.getChild("offensive").getValue());
						win.description.setValue(sourceNode.getChild("description").getValue());
						
						win.locked.setValue(1);
					end
				end
				
				if not bFoundSpell then
					Debug.chat(sAbility);
				end
				
			end
		end
end

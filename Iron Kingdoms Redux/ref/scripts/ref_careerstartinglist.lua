function onInit()
					
	
	
	local node=window.getDatabaseNode().getChild("startingtable");
	
	local parsestring="";
	if node.getChild("parsestring") then
		parsestring=node.getChild("parsestring").getValue();
	end
	
	local assetsstring=""
	if node.getChild("assetsstring") then
		assetsstring=node.getChild("assetsstring").getValue();
	end
	
	local moduletarget=""
	if node.getChild("moduletarget") then
		moduletarget=node.getChild("moduletarget").getValue();
	end
	
	--Debug.chat(parsestring);
	
	if parsestring~="" then
		
		
		local labels =StringManager.split(parsestring, ";") ;
		local nStarts, nEnds, sLabel, sText
		
		for i,categorytext in pairs(labels) do
		
			local win=createWindow();
			
			nStarts, nEnds, sLabel, sText = string.find(Utility.trim(categorytext) , "([%w%s]*:)%s(.*)");
			
			
			win.name.setValue(sLabel);
		
			win.text.setValue(Utility.trim(sText));
			
		end
		
		if assetsstring~="" then
		
			
			local win=createWindow();
			win.name.setValue("Starting Assets:");		
			win.text.setValue(Utility.trim(assetsstring));
		
		end
		--[[
		local labels =StringManager.split(parsestring, ",") ;
		
		local nStarts, nEnds, sSkill, sMax, sSpec;
		
		for i,skillname in pairs(labels) do
			local win=createWindow();
			
			
			
			nStarts, nEnds, sSkill, sMax, sSpec = string.find(trim(skillname) , "([%w%s]*)%s(%d+)%s?([%w%s/(/)]*)");
			
			
			if sMax then
				win.max.setValue(sMax);
			else
				win.max.setVisible(false);
			end
			
			--Debug.chat(trim(skillname));			
			--Debug.chat(sSkill);
			--Debug.chat(sMax);
			--if sSpec~="" then Debug.chat(sSpec); end
			
			
			if sSkill~=nil and string.lower(trim(sSkill))~="general skills" then
				
				if skilltype=="militaryskills" then
					
					win.name.setValue( trim(sSkill).." "..sSpec )
					local link = "reference.skills.groups.military.index."..parseNodeName(sSkill)..moduletarget;
					win.listlink.setValue("referenceskill",link);
					
				end
				
				if skilltype=="occupationalskills" then
					
					win.name.setValue( trim(sSkill).." "..sSpec )
					local link = "reference.skills.groups.occupational.index."..parseNodeName(sSkill)..moduletarget;
					win.listlink.setValue("referenceskill",link);
				
				end
			
			else		
				
				if sSkill~=nil then
					win.name.setValue( trim(sSkill).." "..sSpec )
					local link = "reference.skills"..moduletarget;
					win.listlink.setValue("reference_skilltablelist",link);
				else
					win.name.setValue( "None")
					win.listlink.setVisible(false);
				end
				
			end
			
			
			
			
		end
		]]
		
	else
		setDatabaseNode( DB.findNode(moduletarget) );		



	end


end


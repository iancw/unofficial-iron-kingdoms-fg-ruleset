-- 
-- Please see the license.html file included with this distribution for 
-- attribution and copyright information.
--

local srcnode = nil;
local srcnodename = "";

local cycleindex = 1;

local labels = {};
local values = {};

local sFont = "sheettext";
local sFontColor = "";
local widgetText = nil;

local labelControl=nil;
local valueControl=nil;

function onInit()
	-- GET PARAMETERS
	if font then
		sFont = font[1];
	end
	if color then
		sFontColor = color[1];
	end
	
	if labeltarget then
		labelControl = window[labeltarget[1]];
	end
	if valuetarget then
		valueControl = window[valuetarget[1]];
	end
	
	local sLabels = "";
	local sValues = "";
	
	sLabels=labelControl.getValue();
	sValues=valueControl.getValue();
	
	
	
	initialize(sLabels, sValues);
	
	-- SET ACCESS RIGHTS
	local bLocked = false;
	
	-- SET UP DATA CONNECTION
	if not sourceless then
		srcnodename = getName();
		if source and source[1] and source[1].name then
			srcnodename = source[1].name[1];
		end
	end
	if srcnodename ~= "" then
		-- DETERMINE DB READ-ONLY STATE
		local node = window.getDatabaseNode();
		if node.isReadOnly() then
			bLocked = true;
		end

		-- LINK TO DATABASE NODE, AND FUTURE UPDATES
		srcnode = node.createChild(srcnodename, "string");
		if srcnode then
			srcnode.onUpdate = update;
		elseif node then
			node.onChildAdded = registerUpdate;
		end
		
		-- SYNCHRONIZE DATA VALUES
		synchData();
	end

	-- Set the correct read only value
	if bLocked then
		setReadOnly(bLocked);
	end
	
	if widgetText then widgetText.destroy(); end
	-- SET UP TEXT WIDGET
	widgetText = addTextWidget(sFont, "");
	widgetText.setPosition("center", 0, 0);
	if sFontColor and sFontColor ~= "" then
		widgetText.setColor(sFontColor);
	end

	-- UPDATE DISPLAY
	updateDisplay();
	
	
	DB.addHandler(labelControl.getDatabaseNode().getNodeName(), "onUpdate", onInit);
	DB.addHandler(valueControl.getDatabaseNode().getNodeName(), "onUpdate", onInit);
	
end

function onClose()
	DB.removeHandler(labelControl.getDatabaseNode().getNodeName(), "onUpdate", onInit);
	DB.removeHandler(valueControl.getDatabaseNode().getNodeName(), "onUpdate", onInit);
end

function registerUpdate(nodeSource, nodeChild)
	if nodeChild.getName() == srcnodename then
		nodeSource.onChildAdded = function () end;
		nodeChild.onUpdate = update;
		srcnode = nodeChild;
		update();
	end
end


function initialize(sLabels, sValues, sDefaultLabel, sInitialValue)
	if sLabels then
		labels = StringManager.split(sLabels, "|");
	end
	
	if sValues then
		values = StringManager.split(sValues, "|");
	end
		
	if sInitialValue then
		matchData(sInitialValue);
	end
end


function matchData(sValue)
	local nMatch = 0;
	for k,v in pairs(values) do
		if v == sValue then
			nMatch = k;
		end
	end

	if nMatch > 0 then
		cycleindex = nMatch;
	else
		cycleindex = 1;
	end
end

function synchData()
	local srcval = "";
	if srcnode then
		srcval = srcnode.getValue();
	end
	
	matchData(srcval);
end

function updateDisplay()
	if cycleindex > 0 and cycleindex <= #labels then
		widgetText.setText(labels[cycleindex]);
	else
		widgetText.setText("#NOVAL");
	end
end

function update()
	synchData();
	updateDisplay();

	if self.onValueChanged then
		self.onValueChanged();
	end
end

function getDatabaseNode()
	return srcnode;
end

function setStringValue(srcval)
	if srcnode then
		srcnode.setValue(srcval);
	else
		matchData(srcval);
		updateDisplay();
		if self.onValueChanged then
			self.onValueChanged();
		end
	end
end

function getValue()
	if cycleindex > 0 and cycleindex <= #labels then
		return labels[cycleindex];
	end
	
	return "";
end

function getStringValue()
	if cycleindex > 0 and cycleindex <= #values then
		return values[cycleindex];
	end
	
	return "";
end

function setDisplayColor(sColor)
	widgetText.setColor(sColor);
end

function setDisplayFont(sFont)
	widgetText.setFont(sFont);
end

function cycleLabel(bForward)
	if bForward then
		if cycleindex < #labels then
			cycleindex = cycleindex + 1;
		else
			cycleindex = 1;
		end
	else
		if cycleindex > 1 then
			cycleindex = cycleindex - 1;
		else
			cycleindex = #labels;
		end
	end

	if srcnode then
		srcnode.setValue(getStringValue());
	else
		updateDisplay();
		if self.onValueChanged then
			self.onValueChanged();
		end
	end
end

function onClickDown(button, x, y)
	return true;
end

function onClickRelease(button, x, y)
	if not isReadOnly() then
		cycleLabel(true);
	end
	return true;
end
